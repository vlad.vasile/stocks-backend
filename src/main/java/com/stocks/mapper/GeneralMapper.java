package com.stocks.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.stocks.domain.Offer;
import com.stocks.domain.Portofolio;
import com.stocks.model.OfferModel;
import com.stocks.model.PortofolioModel;

@Mapper(config = StocksMapstructConfig.class)
public interface GeneralMapper {
  GeneralMapper mapper = Mappers.getMapper(GeneralMapper.class);

  OfferModel toOfferModel(Offer offer);

  PortofolioModel toPortofolioModel(Portofolio left);

}
