package com.stocks.domain;

import lombok.Data;

@Data
public class Stock {
  public Long id;
  public String name;
  public String holdings;
}
