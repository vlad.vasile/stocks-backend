package com.stocks.domain;

import java.util.List;

import lombok.Data;

@Data
public class Portofolio {
  public Long id;
  public String name;
  public List<Stock> stocks;

  public Portofolio add(Stock stock) {
    this.stocks = io.vavr.collection.List.ofAll(this.stocks).append(stock).distinctBy(x -> x.id).asJava();
    return this;
  }

  public Portofolio remove(Stock stock) {
    this.stocks = io.vavr.collection.List.ofAll(this.stocks).remove(stock).asJava();
    return this;
  }
}
