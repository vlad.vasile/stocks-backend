package com.stocks.domain;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.NONE)
@ToString
public class Offer {
  public final List<Portofolio> portofolios;
  public final List<Stock> availableStocks;

  public static Offer from(Portofolios portofolios, Stocks stocks) {
    return new Offer(portofolios.portofolios, stocks.availableStocks);
  }
}
