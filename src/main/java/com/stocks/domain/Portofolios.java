package com.stocks.domain;

import java.util.List;
import java.util.Optional;

import lombok.Data;

@Data
public class Portofolios {
  public final List<Portofolio> portofolios;

  public Optional<Portofolio> findPortofolio(Long portofolioId) {
    return portofoliosVavr().find(x -> x.id.equals(portofolioId)).toJavaOptional();
  }

  private io.vavr.collection.List<Portofolio> portofoliosVavr() {
    return io.vavr.collection.List.ofAll(portofolios);
  }

  public Portofolio updateAndGet(Long portofolioId, Stock stock) {
    Portofolio portofolio = getPortofolio(portofolioId);
    return portofolio.add(stock);
  }

  public Portofolio getPortofolio(Long portofolioId) {
    return portofoliosVavr().find(x -> x.id.equals(portofolioId)).get();
  }

  public Portofolio deleteAndGet(Long portofolioId, Stock stock) {
    Portofolio portofolio = getPortofolio(portofolioId);
    return portofolio.remove(stock);
  }
}
