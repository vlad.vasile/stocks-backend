package com.stocks.domain;

import java.util.List;

import lombok.Data;

@Data
public class Stocks {
  public List<Stock> availableStocks;

  public Stock getStock(Long stockId) {
    return io.vavr.collection.List.ofAll(availableStocks).find(x -> x.id.equals(stockId)).get();
  }
}
