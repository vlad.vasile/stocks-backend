package com.stocks.service;

import com.stocks.model.OfferModel;
import com.stocks.model.PortofolioModel;

import io.vavr.control.Either;

public interface OfferService {

  OfferModel getOffer();

  Either<PortofolioModel, String> findPortofolio(Long portofolioId);

  PortofolioModel updatePortofolio(Long portofolioId, Long stockId);

  PortofolioModel deleteStock(Long portofolioId, Long stockId);

}
