package com.stocks.service;

import com.stocks.domain.Offer;
import com.stocks.domain.Portofolio;

import io.vavr.control.Either;

public interface OfferStorage {
  Offer getOffer();

  Either<Portofolio, String> findPortofolio(Long portofolioId);

  Portofolio savePortofolio(Portofolio portofolio);

  Portofolio updatePortofolio(Long portofolioId, Long stockId);

  Portofolio deleteStock(Long portofolioId, Long stockId);
}
