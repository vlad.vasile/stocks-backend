package com.stocks.service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.raisercostin.jedio.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.stocks.domain.Offer;
import com.stocks.domain.Portofolio;
import com.stocks.domain.Portofolios;
import com.stocks.domain.Stock;
import com.stocks.domain.Stocks;

import io.vavr.collection.List;
import io.vavr.control.Either;

@Component
public class OfferStorageImpl implements OfferStorage {

  private static final String STOCKS_FILE = "available-stocks.json";
  private static final String PORTOFOLIOS_FILE = "portofolios.json";
  private final Gson parser = new Gson();
  private Map<Long, Portofolio> portofolios = new ConcurrentHashMap<Long, Portofolio>();

  @Autowired
  public OfferStorageImpl() {
    Portofolios portofoliosGroup = initLoadPortofoliosFromFile();
    List.ofAll(portofoliosGroup.portofolios).forEach(x -> this.portofolios.put(x.id, x));
  }

  @Override
  public Offer getOffer() {
    Stocks stocks = loadStocks();
    Portofolios portofolios = new Portofolios(List.ofAll(this.portofolios.values()).asJava());
    return Offer.from(portofolios, stocks);
  }

  private Portofolios initLoadPortofoliosFromFile() {
    return parse(PORTOFOLIOS_FILE, Portofolios.class);
  }

  private Stocks loadStocks() {
    return parse(STOCKS_FILE, Stocks.class);
  }

  private <T> T parse(String path, Class<T> clazz) {
    String content = Locations.classpath(path).asReadableFile().readContent();
    return parser.fromJson(content, clazz);
  }

  @Override
  public Portofolio savePortofolio(Portofolio portofolio) {
    throw new RuntimeException("Not implemented yet!");
  }

  @Override
  public Either<Portofolio, String> findPortofolio(Long portofolioId) {
    Portofolios portofolios = new Portofolios(List.ofAll(this.portofolios.values()).asJava());
    Optional<Portofolio> portofolioOpt = portofolios.findPortofolio(portofolioId);
    if (portofolioOpt.isPresent()) {
      return Either.left(portofolioOpt.get());
    } else {
      return Either.right("Portofolio with id " + portofolioId + " not found!");
    }
  }

  @Override
  public Portofolio updatePortofolio(Long portofolioId, Long stockId) {
    Portofolios portofoliosOffer = new Portofolios(List.ofAll(this.portofolios.values()).asJava());
    Stock stock = loadStocks().getStock(stockId);
    Portofolio portofolio = portofoliosOffer.updateAndGet(portofolioId, stock);
    this.portofolios.put(portofolioId, portofolio);
    return portofolio;
  }

  @Override
  public Portofolio deleteStock(Long portofolioId, Long stockId) {
    Portofolios portofoliosOffer = new Portofolios(List.ofAll(this.portofolios.values()).asJava());
    Stock stock = loadStocks().getStock(stockId);
    Portofolio portofolio = portofoliosOffer.deleteAndGet(portofolioId, stock);
    this.portofolios.put(portofolioId, portofolio);
    return portofolio;
  }

}
