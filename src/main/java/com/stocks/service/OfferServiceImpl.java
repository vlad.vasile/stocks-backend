package com.stocks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stocks.domain.Offer;
import com.stocks.domain.Portofolio;
import com.stocks.mapper.GeneralMapper;
import com.stocks.model.OfferModel;
import com.stocks.model.PortofolioModel;

import io.vavr.control.Either;

@Component
public class OfferServiceImpl implements OfferService {
  private static final GeneralMapper mapper = GeneralMapper.mapper;

  @Autowired
  private OfferStorage offerStorage;

  @Override
  public OfferModel getOffer() {
    Offer offer = offerStorage.getOffer();
    return mapper.toOfferModel(offer);
  }

  @Override
  public Either<PortofolioModel, String> findPortofolio(Long portofolioId) {
    Either<Portofolio, String> portofolio = offerStorage.findPortofolio(portofolioId);
    if (portofolio.isLeft()) {
      return Either.left(mapper.toPortofolioModel(portofolio.getLeft()));
    } else {
      return Either.right(portofolio.get());
    }
  }

  @Override
  public PortofolioModel updatePortofolio(Long portofolioId, Long stockId) {
    Portofolio portofolio = offerStorage.updatePortofolio(portofolioId, stockId);
    return mapper.toPortofolioModel(portofolio);
  }

  @Override
  public PortofolioModel deleteStock(Long portofolioId, Long stockId) {
    Portofolio portofolio = offerStorage.deleteStock(portofolioId, stockId);
    return mapper.toPortofolioModel(portofolio);
  }

}
