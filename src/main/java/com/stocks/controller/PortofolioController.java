package com.stocks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stocks.model.PortofolioModel;
import com.stocks.service.OfferService;
import com.stocks.ui.rest.GenericController;
import com.stocks.ui.rest.RestResponse;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/portofolios")
public class PortofolioController extends GenericController {
  @Autowired
  private OfferService offerService;

  @RequestMapping(method = RequestMethod.GET, path = "/{portofolioId}")
  ResponseEntity<RestResponse<PortofolioModel>> getPortofolio(@PathVariable(name = "portofolioId") Long portofolioId) {
    Either<PortofolioModel, String> portofolio = offerService.findPortofolio(portofolioId);
    if (portofolio.isLeft()) {
      return ok("get offer", portofolio.getLeft());
    } else {
      return error(HttpStatus.NOT_FOUND, portofolio.get());
    }
  }

  @RequestMapping(method = RequestMethod.POST, path = "/{portofolioId}")
  ResponseEntity<RestResponse<PortofolioModel>> updatePortofolio(
      @PathVariable(name = "portofolioId") Long portofolioId,
      @RequestParam(name = "stockId", required = true) Long stockId) {
    PortofolioModel portofolio = offerService.updatePortofolio(portofolioId, stockId);
    return ok("portofolio" + portofolioId + " updated", portofolio);
  }

  @RequestMapping(method = RequestMethod.DELETE, path = "/{portofolioId}/stocks/{stockId}")
  ResponseEntity<RestResponse<PortofolioModel>> deleteStockFromPortofolio(
      @PathVariable(name = "portofolioId") Long portofolioId,
      @PathVariable(name = "stockId") Long stockId) {
    PortofolioModel portofolio = offerService.deleteStock(portofolioId, stockId);
    return ok("portofolio" + portofolioId + " updated", portofolio);
  }
}
