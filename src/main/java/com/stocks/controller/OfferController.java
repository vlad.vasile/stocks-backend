package com.stocks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stocks.model.OfferModel;
import com.stocks.service.OfferService;
import com.stocks.ui.rest.GenericController;
import com.stocks.ui.rest.RestResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/offer")
public class OfferController extends GenericController {

  @Autowired
  private OfferService offerService;

  @RequestMapping(method = RequestMethod.GET)
  ResponseEntity<RestResponse<OfferModel>> getOffer() {
    return ok("get offer", offerService.getOffer());
  }
}
