package com.stocks.app;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.databind.Module;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.TreeMap;
import io.vavr.jackson.datatype.VavrModule;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication(scanBasePackages = { "com.stocks.*" })
@EnableAutoConfiguration
@PropertySources({
    @PropertySource(value = "classpath:/application.properties") })
//@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@Slf4j
public class StocksBackendApplication {
  @Autowired
  Environment env;
  @Autowired
  ApplicationContext applicationContext;

  public static void main(String[] args) {
    SpringApplication.run(StocksBackendApplication.class, args);
  }

  @PostConstruct
  public void init() throws Exception {
    showInfoProperties();
    showJVMParameters();
  }

  @Bean
  Module vavrModule() {
    return new VavrModule();
  }

  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {
    log.info("hello world, I have just started up");
  }

  public static Map<String, Object> getAllKnownProperties(Environment env) {
    Map<String, Object> rtn = TreeMap.empty();
    if (env instanceof ConfigurableEnvironment) {
      for (org.springframework.core.env.PropertySource<?> propertySource : ((ConfigurableEnvironment) env)
        .getPropertySources()) {
        if (propertySource instanceof EnumerablePropertySource) {
          for (String key : ((EnumerablePropertySource) propertySource).getPropertyNames()) {
            rtn = rtn.put(key + "@" + propertySource.getName(), propertySource.getProperty(key));
          }
        }
      }
    }
    return rtn;
  }

  private void showInfoProperties() {
    log.info(
      "*********************Please check out this link if you need to override bellow application properties:\nhttps://docs.spring.io/spring-boot/docs/1.5.22.RELEASE/reference/html/boot-features-external-config.html\n\nProperties:\n"
          + getAllKnownProperties(env).map(x -> "    " + x._1() + "=" + x._2 + "\n").mkString()
          + "*********************\n");
    if (log.isDebugEnabled()) {
      log.debug("\n\n\nBeans:\n**********\nbean:" 
          + List.of(applicationContext.getBeanDefinitionNames()).mkString("\nbean:"));
    }
  }

  private void showJVMParameters() {
    log.info(">>>>>>>>>>>>>>>>> JVM PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<");
    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    java.util.List<String> jvmArgs = runtimeMXBean.getInputArguments();
    for (String arg : jvmArgs) {
      log.info(arg);
    }
    log.info(">>>>>>>>>>>>>>>>> JVM PARAMETERS END <<<<<<<<<<<<<<<<<<<<<<<<");
  }

}
