package com.stocks.model;

import java.util.List;

import lombok.Data;

@Data
public class OfferModel {
  public List<PortofolioModel> portofolios;
  public List<StockModel> availableStocks;
}
