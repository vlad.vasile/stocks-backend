package com.stocks.model;

import java.util.List;

import lombok.Data;

@Data
public class PortofolioModel {
  public Long id;
  public String name;
  public List<StockModel> stocks;
}
