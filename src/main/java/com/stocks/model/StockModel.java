package com.stocks.model;

import lombok.Data;

@Data
public class StockModel {
  public Long id;
  public String name;
  public String holdings;
}
