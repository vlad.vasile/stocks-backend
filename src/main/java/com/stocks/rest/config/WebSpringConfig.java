package com.stocks.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebSpringConfig {

  @Bean
  public WebMvcConfigurer webMvcConfigurer() {
    return new WebMvcConfigurer()
      {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping("/**").allowedOrigins("*").allowedMethods("*");
        }

        @Override
        public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
          configurer.ignoreAcceptHeader(true);
          configurer.defaultContentType(MediaType.APPLICATION_JSON);
        }
      };
  }

}