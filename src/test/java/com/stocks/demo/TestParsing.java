package com.stocks.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.raisercostin.jedio.Locations;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.stocks.domain.Stocks;

public class TestParsing {

  @Test
  void testParsing() {
    String content = Locations.classpath("available-stocks.json").asReadableFile().readContent();
    assertFalse(Strings.isNullOrEmpty(content));

    Stocks stocks = new Gson().fromJson(content, Stocks.class);
    assertNotNull(stocks);
    assertEquals("HSBC", stocks.availableStocks.get(0).name);
  }
}
