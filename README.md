## Stocks-backend

## Install notes
1. install eclipse: m2e-apt plugin
    - Help > Eclipse Marketplace ... search m2e-apt
    - eclipse enable annotation processor - https://manios.org/2017/08/09/configure-eclipse-in-order-to-build-mapstruct-in-java-projects
3. copy/use `src\main\resources\static\lombok-1.18.10--withMapstruct.jar` to `<eclipse>\lombok-1.18.10--withMapstruct.jar`
4. Edit `<eclipse>\eclipse.ini` the following last line `-javaagent:lombok-1.18.10--withMapstruct.jar`
5. Download sources and run the app. 
    - Check **BASE_URL** = `http://localhost:8089`